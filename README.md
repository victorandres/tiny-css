# tiny css

This library is a simple and lightweight bootstrap-like grid system for layouts :)
## CDN

https://cdn.tinycss.itza.xyz/v1.0.0/tinycssmin.css

Or clone/download file in the repository

### Docs

The grid system is totally identical of bootstrap for twitter, if need help read the documentation from them

https://getbootstrap.com/docs/4.4/layout/grid/

#### Thanks :)

Thanks for use, you can make pull request for fixes/improvements